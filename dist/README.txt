Menu Launcher for Kindle Touch

Version 1.2.2 for KT firmware 5.1.0 by ixtab
Based on work by Yifan Lu (http://yifan.lu/)

Original Source: https://github.com/yifanlu/KindleLauncher
Updated Source: https://bitbucket.org/ixtab/ktlauncher

This mod adds a "Launcher" menu option to the home screen's menu and the book reader's menu.
Choosing the "Launcher" option will open a new menu that is customizable.

Place extensions into their own folder under the extensions directory on the device's USB drive.
Each extension MUST have a "config.xml" file. You can check here: http://yifanlu.github.com/KindleLauncher/com/yifanlu/Kindle/ExtensionsLoader.html to see what to put into "config.xml".
Or you can check out the provided extensions and see for yourself.

If you make a JSON menu extension, you can see the example extension's menu options or read this: http://yifanlu.github.com/KindleLauncher/com/yifanlu/Kindle/JSONMenu.html#JSONMenu(java.io.File)
If you make a Java menu extension, import the Kindle's JARs along with KindleLauncher.jar into your Java IDE and create a class that implements the com.yifanlu.Kindle.Menuable interface.
More information: http://yifanlu.github.com/KindleLauncher/com/yifanlu/Kindle/Menuable.html


